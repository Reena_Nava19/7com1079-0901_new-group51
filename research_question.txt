Group: new_group51

Question
========

RQ: Is there a difference in the median final grade of students among different levels of alcohol consumed on weekdays?

Alternative hypothesis: : There is a difference in the median final grade of students among different levels of alcohol consumed on workdays.

Null hypothesis: There is no difference in the median final grade of students among different levels of alcohol consumed on workdays.


Dataset
=======

URL: https://www.kaggle.com/uciml/student-alcohol-consumption

Column Headings:

```
> View(student_mat)
> student_mat<- read.csv("C:/Users/BHASKAR/Downloads/New folder/student-mat.csv")
> colnames(student_mat)

 [1] "school"     "sex"        "age"        "address"    "famsize"   
 [6] "Pstatus"    "Medu"       "Fedu"       "Mjob"       "Fjob"      
[11] "reason"     "guardian"   "traveltime" "studytime"  "failures"  
[16] "schoolsup"  "famsup"     "paid"       "activities" "nursery" 

..




